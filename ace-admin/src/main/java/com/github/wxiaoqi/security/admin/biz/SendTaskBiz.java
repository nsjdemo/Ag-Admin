package com.github.wxiaoqi.security.admin.biz;


import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.admin.mapper.SendTaskMapper;
import com.github.wxiaoqi.security.api.entity.SendTask;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 
 *
 * @author Mr.AG
 * @email 463540703@qq.com
 * @date 2018-05-29 10:34:50
 */
@Service
public class SendTaskBiz extends BaseBiz<SendTaskMapper,SendTask> {
}