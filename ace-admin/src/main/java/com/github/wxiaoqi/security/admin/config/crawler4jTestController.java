package com.github.wxiaoqi.security.admin.config;

import com.github.wxiaoqi.security.auth.client.annotation.IgnoreClientToken;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author anchunming
 * @create 2018/8/13 0013 上午 10:16
 */
@Controller
public class crawler4jTestController {

    @IgnoreClientToken
    @RequestMapping(value = "crawler4jtest",method = RequestMethod.GET)
    public void crawler4j() throws Exception {

        String crawlStorageFolder = "/database";//文件存储位置
        int numberOfCrawlers = 1;//线程数量
        System.out.println("==================================");

        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(crawlStorageFolder);//配置信息设置
        //config.setMaxPagesToFetch(1);//设置爬取页面数

        config.setMaxDepthOfCrawling(1);//爬取深度
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);   //创建爬虫执行器

        //controller.addSeed("https://search.jd.com/Search?keyword=笔记本&enc=utf-8&wq=笔记本");    //传入种子 要爬取的网址
        controller.addSeed("http://www.cuncunle.com/village-317787.html");
        controller.addSeed("http://www.cuncunle.com/village-317713.html");

        controller.start(MyCrawler.class, numberOfCrawlers);
    }

}
