package com.github.wxiaoqi.security.admin.rpc.service;

import com.github.wxiaoqi.security.admin.biz.ElementBiz;
import com.github.wxiaoqi.security.admin.biz.MenuBiz;
import com.github.wxiaoqi.security.admin.biz.UserBiz;
import com.github.wxiaoqi.security.admin.biz.VillageUserBiz;
import com.github.wxiaoqi.security.admin.constant.AdminCommonConstant;
import com.github.wxiaoqi.security.admin.entity.Element;
import com.github.wxiaoqi.security.admin.entity.Menu;
import com.github.wxiaoqi.security.admin.entity.User;
import com.github.wxiaoqi.security.admin.mapper.VillageUserMapper;
import com.github.wxiaoqi.security.admin.util.Md5Util;
import com.github.wxiaoqi.security.admin.vo.FrontUser;
import com.github.wxiaoqi.security.admin.vo.MenuIcon;
import com.github.wxiaoqi.security.admin.vo.MenuTree;
import com.github.wxiaoqi.security.api.authority.PermissionInfo;
import com.github.wxiaoqi.security.api.user.UserInfo;
import com.github.wxiaoqi.security.auth.client.jwt.UserAuthUtil;
import com.github.wxiaoqi.security.common.constant.CommonConstants;
import com.github.wxiaoqi.security.common.util.TreeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ace on 2017/9/12.
 */
@Service
public class PermissionService {
    @Autowired
    private UserBiz userBiz;
    @Autowired
    private MenuBiz menuBiz;
    @Autowired
    private ElementBiz elementBiz;
    @Autowired
    private UserAuthUtil userAuthUtil;
    @Autowired
    private VillageUserBiz villageUserBiz;

    @Autowired
    private VillageUserMapper villageUserMapper;

    @Value("${key}")
    private String Md5key;

    public UserInfo getUserByUsername(String username)throws Exception{
        UserInfo info = new UserInfo();
        User user = userBiz.getUserByUsername(username);
        Md5Util md5Util=new Md5Util(Md5key);
        String passwordb = md5Util.decrypt(user.getPassword());
        BeanUtils.copyProperties(user, info);
        info.setId(user.getId().toString());
        info.setStatus(user.getStatus());
        info.setPassword(passwordb);

        return info;
    }

    public List<PermissionInfo> getAllPermission() {
        List<Menu> menus = menuBiz.selectListAll();
        List<PermissionInfo> result = new ArrayList<PermissionInfo>();
        PermissionInfo info = null;
        menu2permission(menus, result);
        List<Element> elements = elementBiz.selectListAll();
        element2permission(result, elements);
        return result;
    }

    private void menu2permission(List<Menu> menus, List<PermissionInfo> result) {
        PermissionInfo info;
        for (Menu menu : menus) {
            if (StringUtils.isBlank(menu.getHref())) {
                menu.setHref("/" + menu.getCode());
            }
            info = new PermissionInfo();
            info.setCode(menu.getCode());
            info.setType(AdminCommonConstant.RESOURCE_TYPE_MENU);
            info.setName(AdminCommonConstant.RESOURCE_ACTION_VISIT);
            String uri = menu.getHref();
            if (!uri.startsWith("/")) {
                uri = "/" + uri;
            }
            info.setUri(uri);
            info.setMethod(AdminCommonConstant.RESOURCE_REQUEST_METHOD_GET);
            result.add(info
            );
            info.setMenu(menu.getTitle());
        }
    }

    public List<PermissionInfo> getPermissionByUsername(String username) {
        User user = userBiz.getUserByUsername(username);
        List<Menu> menus = menuBiz.getUserAuthorityMenuByUserId(1);
        List<PermissionInfo> result = new ArrayList<PermissionInfo>();
        PermissionInfo info = null;
        menu2permission(menus, result);
        List<Element> elements = elementBiz.getAuthorityElementByUserId(1 + "");
        element2permission(result, elements);
        return result;
    }

    private void element2permission(List<PermissionInfo> result, List<Element> elements) {
        PermissionInfo info;
        for (Element element : elements) {
            info = new PermissionInfo();
            info.setCode(element.getCode());
            info.setType(element.getType());
            info.setUri(element.getUri());
            info.setMethod(element.getMethod());
            info.setName(element.getName());
            info.setMenu(element.getMenuId());
            result.add(info);
        }
    }


    private List<MenuTree> getMenuTree(List<Menu> menus, int root) {
        List<MenuTree> trees = new ArrayList<MenuTree>();
        MenuTree node = null;
        for (Menu menu : menus) {
            node = new MenuTree();
            BeanUtils.copyProperties(menu, node);
            trees.add(node);
        }
        return TreeUtil.bulid(trees, root);
    }

    public FrontUser getUserInfo(String token) throws Exception {
        String username = userAuthUtil.getInfoFromToken(token).getUniqueName();
        if (username == null) {
            return null;
        }
        UserInfo user = this.getUserByUsername(username);
        FrontUser frontUser = new FrontUser();
        BeanUtils.copyProperties(user, frontUser);
        List<PermissionInfo> permissionInfos = this.getPermissionByUsername(username);
        Stream<PermissionInfo> menus = permissionInfos.parallelStream().filter((permission) -> {
            return permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
        });
        frontUser.setMenus(menus.collect(Collectors.toList()));
        Stream<PermissionInfo> elements = permissionInfos.parallelStream().filter((permission) -> {
            return !permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
        });
        frontUser.setElements(elements.collect(Collectors.toList()));
        return frontUser;
    }

    public List<MenuTree> getMenusByUsername(String token) throws Exception {

        String username = userAuthUtil.getInfoFromToken(token).getUniqueName();
        if (username == null) {
            return null;
        }
        if (username.equals("admin")){
            // TODO: 18/8/15 管理品台
            User user = userBiz.getUserByUsername(username);
            List<Menu> menus = menuBiz.getUserAuthorityMenuByUserId(1);
            return getMenuTree(menus,AdminCommonConstant.ROOT);
        }else{
            // TODO: 18/8/15 村庄品台
            User user = userBiz.getUserByUsername(username);
            List<Menu> menus = menuBiz.getVillageUserAuthorityMenuByUserId(1);
            return getMenuTree(menus,AdminCommonConstant.ROOT);
        }

    }

    public List<Menu> getMenuByUserId(Integer userId) throws Exception {
        List<Menu> menus = menuBiz.getUserAuthorityMenuByUserId(userId);
        List<Menu> resultMenus = new ArrayList<>();
        menus.forEach(i -> {
        	if("menu".equals(i.getType())&&StringUtils.isEmpty(i.getAttr2())){
        		resultMenus.add(i);
        	}
        });
        return resultMenus;
    }

    public List<MenuIcon> getIcons(String token) throws Exception {

        String username = userAuthUtil.getInfoFromToken(token).getUniqueName();
        System.out.println("--------------"+username);
        if (username == null) {
            return null;
        }
        if (username.equals("admin")){
            // TODO: 18/8/15 管理平台
            return menuBiz.getAdminIcon(1);
        }else{
            // TODO: 18/8/15 村庄平台
            return menuBiz.getAdminIcon(2);
        }

    }
}
