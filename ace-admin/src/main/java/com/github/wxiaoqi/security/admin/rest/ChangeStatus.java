package com.github.wxiaoqi.security.admin.rest;

import com.github.wxiaoqi.security.admin.biz.ProjectBiz;
import com.github.wxiaoqi.security.admin.vo.ProjectResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
@Component
public class ChangeStatus {
    @Autowired
    private ProjectBiz projectBiz;
    //@Scheduled(cron = "0 0/1 * * * ?")
    @Scheduled(cron = "0 0 0 * * ?")//每天凌晨执行一次
    public void changeStatus(){
        List<ProjectResult> unDoneProList = projectBiz.getUnDoneProject();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date sysDate = new Date();
        //遍历上面所查询到的未完成的捐募项目
        for (ProjectResult project:unDoneProList) {
            //项目的截止时间
            Date endTime = null;
            try {
                endTime = df.parse(project.getStopTime());
                //情况1：时间超过了项目结束
                if (endTime.before(sysDate)) {
                    //赋值sql中的#{rate}，#{id}
                    project.setRate("2");
                    project.setId(project.getId());
                    projectBiz.updateUndoneProjectRate(project);
                }

            }catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println("定期执行的定时任务正在运行...");
        }
    }
}
