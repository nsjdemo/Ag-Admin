package com.github.wxiaoqi.security.admin.rest;

import java.util.ArrayList;
import java.util.List;

import com.github.wxiaoqi.security.admin.vo.MenuIcon;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.biz.GroupBiz;
import com.github.wxiaoqi.security.admin.biz.IdentifyCodeBiz;
import com.github.wxiaoqi.security.admin.biz.SmsAlidayuBiz;
import com.github.wxiaoqi.security.admin.biz.UserBiz;
import com.github.wxiaoqi.security.admin.entity.Group;
import com.github.wxiaoqi.security.admin.entity.Menu;
import com.github.wxiaoqi.security.admin.entity.User;
import com.github.wxiaoqi.security.admin.rpc.service.PermissionService;
import com.github.wxiaoqi.security.admin.vo.FrontUser;
import com.github.wxiaoqi.security.admin.vo.MenuTree;
import com.github.wxiaoqi.security.admin.vo.UserVo;
import com.github.wxiaoqi.security.auth.client.annotation.IgnoreClientToken;
import com.github.wxiaoqi.security.common.constant.CommonConstants;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.taobao.api.ApiException;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-08 11:51
 */
@RestController
@RequestMapping("user")
public class UserController extends BaseController<UserBiz,User> {
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private UserBiz userBiz;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);

    @Autowired
    private IdentifyCodeBiz iu;

    @Autowired
    private SmsAlidayuBiz msgService;

    @Autowired
    private GroupBiz groupBiz;

    @Autowired
    StringRedisTemplate redisTemplate;


    Logger logger = Logger.getLogger(UserController.class);
    /**
     * 添加账号
     * @param user
     * @param roleId
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/{roleId}",method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<User> add(@RequestBody User user,@PathVariable Integer roleId) throws Exception{
        Group g = groupBiz.selectById(roleId);
        if("orgRole".equals(g.getCode())){
        }else{//添加管理员账号
            baseBiz.insertSelective(user);
            User user2 = userBiz.getUserByUsername(user.getUsername());
            groupBiz.addRole(roleId,user2.getId());
        }
        return new ObjectRestResponse<User>().rel(true);
    }

    /**
     * 获取角色列表
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/roles", method = RequestMethod.GET)
    @ResponseBody
    public List<Group> getAllRoles() throws Exception {
        List<Group> groups = new ArrayList<>();
        List<Group> groupList =  groupBiz.selectListAll();
        for(Group g : groupList){
            if(!"teacherRole".equals(g.getCode())){
                groups.add(g);
            }
        }
        return groups;
    }

    @RequestMapping(value = "/front/info", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getUserInfo(String token) throws Exception {
        FrontUser userInfo = permissionService.getUserInfo(token);
        if(userInfo==null) {
            return ResponseEntity.status(401).body(false);
        } else {
            return ResponseEntity.ok(userInfo);
        }
    }

    @RequestMapping(value = "/front/menus", method = RequestMethod.GET)
    public @ResponseBody
    List<MenuTree> getMenusByUsername(String token) throws Exception {
        return permissionService.getMenusByUsername(token);
    }

    @RequestMapping(value = "/icons")
    @ResponseBody
    public List<MenuIcon> getIcons(String token) throws Exception{

        return permissionService.getIcons(token);

    }

    @RequestMapping(value = "/front/menu/{userId}", method = RequestMethod.GET)
    public @ResponseBody
    List<Menu> getMenuByUserId(@PathVariable Integer userId) throws Exception {
        return permissionService.getMenuByUserId(userId);
    }

    @IgnoreClientToken
    @PostMapping("/phone/{cellPhone}")
    @ResponseBody
    public ObjectRestResponse<User> phoneSms(@PathVariable(value = "cellPhone") String cellPhone){

        if(userBiz.phoneExist(cellPhone)){
            return new ObjectRestResponse<User>().rel(false).data("手机号已注册！");
        }else{
          String randomkey = String.format(CommonConstants.RANDOMKEY, cellPhone);
          int random = (int) ((Math.random() * 9 + 1) * 100000);
          try {
              msgService.sendLoginVerifySMS(cellPhone, String.valueOf(random));
              ValueOperations<String, String> ops = redisTemplate.opsForValue();
              ops.set(randomkey, String.valueOf(random));
              return new ObjectRestResponse<User>().rel(true).data("短信发送成功！");
          } catch (ApiException e) {
              return new ObjectRestResponse<User>().rel(false).data("短信发送失败！");
          }

        }

    }
    @IgnoreClientToken
    @PostMapping("/phone/valid/{cellPhone}")
    @ResponseBody
    public ObjectRestResponse<User> phoneValid(@PathVariable(value = "cellPhone") String cellPhone){

        if(userBiz.phoneExist(cellPhone)){
          String randomkey = String.format(CommonConstants.RANDOMKEY, cellPhone);
          int random = (int) ((Math.random() * 9 + 1) * 100000);
          try {
              msgService.sendLoginVerifySMS(cellPhone, String.valueOf(random));
              ValueOperations<String, String> ops = redisTemplate.opsForValue();
              ops.set(randomkey, String.valueOf(random));
              return new ObjectRestResponse<User>().rel(true).data("短信发送成功！");
          } catch (ApiException e) {
              return new ObjectRestResponse<User>().rel(false).data("短信发送失败！");
          }

        }else{

            return new ObjectRestResponse<User>().rel(false).data("手机号不存在！");

        }

    }

    @IgnoreClientToken
    @PostMapping("/forget")
    @ResponseBody
    public ObjectRestResponse<User> forgetPwd(@RequestBody UserVo uvo){
        if(!iu.identifyCode(uvo.getPhone(), uvo.getIdentifyCode())){
            return new ObjectRestResponse<User>().rel(false).data("验证码错误");
        }else{
            User user = new User();
            user.setMobilePhone(uvo.getPhone());
            try{
                User ul = userBiz.selectOne(user);
                if(ul==null){
                    return new ObjectRestResponse<User>().rel(false).data("无此用户！");
                }else{
                    ul.setPassword(encoder.encode(uvo.getPassword()));
                    userBiz.updateSelectiveById(ul);
                    return new ObjectRestResponse<User>().rel(true);
                }
            }catch (Exception e){
                return new ObjectRestResponse<User>().rel(false).data("系统错误，请联系管理员!");
            }
        }
    }
    

	@PostMapping("/changemobile")
    @ResponseBody
    public ObjectRestResponse<User> changeMobile(@RequestBody UserVo uvo) {
        if (!iu.identifyCode(uvo.getNewCellPhone(), uvo.getIdentifyCode())) {
            return new ObjectRestResponse<User>().rel(false).data("验证码错误");
        } else {
            String userId = this.getCurrentUserId();
            User user = userBiz.selectById(Integer.parseInt(userId));
            if (encoder.matches(uvo.getPassword(), user.getPassword())) {
                user.setMobilePhone(uvo.getNewCellPhone());
                userBiz.updateById(user);
                return new ObjectRestResponse<User>().rel(true).data("修改成功");
            } else {
                return new ObjectRestResponse<User>().rel(false).data("密码错误");
            }
        }
    }

    @PostMapping("/changepassword")
    @ResponseBody
    public ObjectRestResponse<User> changePassword(@RequestBody UserVo uvo) {
            String userId = this.getCurrentUserId();
            User user = userBiz.selectById(Integer.parseInt(userId));
            if (encoder.matches(uvo.getPassword(), user.getPassword())) {
                user.setPassword(encoder.encode(uvo.getNewPassword()));
                userBiz.updateById(user);
                return new ObjectRestResponse<User>().rel(true).data("修改成功");
            } else {
                return new ObjectRestResponse<User>().rel(false).data("密码错误");
            }

    }

    /**
     * 清空session
     */
    @DeleteMapping("/delSession")
    @ResponseBody
    public void clearSession(){
        logger.info("clear session.....");
    }

}
