package com.github.wxiaoqi.security.admin.config;

import com.github.wxiaoqi.security.admin.biz.VillageProjectBiz;
import com.github.wxiaoqi.security.admin.vo.VillageCommonVO;
import com.github.wxiaoqi.security.api.entity.Village;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * @author anchunming
 * @create 2018/8/13 0013 上午 9:27
 */
public class MyCrawler extends WebCrawler {


    @Resource
    private VillageProjectBiz villageProjectBiz;


    //自定义过滤规则
    private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg|png|mp3|mp4|zip|gz))$");
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();//爬取的网址 转小写
        //这里定义过滤的网址，我的需求是只爬取京东搜索出来的页面中的商品，url需要以https://search.jd.com/search开头
        boolean b =!FILTERS.matcher(href).matches()&&href.startsWith("https://");
        return b;
    }

    /**
    public void visit(Page page) {
        String url = page.getWebURL().getURL();
        System.out.println(url);
        //判断page是否为真正的网页
        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            String html = htmlParseData.getHtml();//页面html内容
            Document doc = Jsoup.parse(html);//采用jsoup解析html，这个大家不会可以简单搜一下

            //使用选择器的时候需要了解网页中的html规则，自己去网页中F12一下，
            Elements elements = doc.select(".gl-item");
            if(elements.size()==0){
                return;
            }
            for (Element element : elements) {
                Elements img = element.select(".err-product");
                if(img!=null){
                    //输出图片链接
                    System.out.println(img.attr("src"));
                    System.out.println(img.attr("data-lazy-img"));
     }
            }
        }
    }*/


    /**
     * 当一个页面被提取并准备好被你的程序处理时，这个函数被调用。
     */
    @Override
    public void visit(Page page) {
        String url = page.getWebURL().getURL();// 获取url
        System.out.println("URL: " + url);
        VillageCommonVO villageCommonVO=new VillageCommonVO();
        List<String> list=new ArrayList();

        if (page.getParseData() instanceof HtmlParseData) {// 判断是否是html数据
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();//// 强制类型转换，获取html数据对象
            String html = htmlParseData.getHtml();//获取页面Html
            //String text = htmlParseData.getText();//获取页面纯文本（无html标签）
            //Set<WebURL> links = htmlParseData.getOutgoingUrls();// 获取页面输出链接

            Document doc = Jsoup.parse(html);
            Elements elements = doc.select(".hamlet_title");
            String villagename = elements.get(0).text();  //村庄名称
            System.out.println(villagename+"======================");
            villageCommonVO.setName(villagename);

            Elements elements1 = doc.select(".mine_addr");
            Elements elements2 = elements1.select("a");
            String address=elements2.get(0).text()+elements2.get(1).text()+elements2.get(2).text()+elements2.get(3).text();//村庄地址
            System.out.println(address+"======================");
            villageCommonVO.setAddress(address);

            Elements elements3 = doc.select(".v_brief_info");
            String description = elements3.get(0).text();  //村庄名称
            System.out.println(description+"======================");
            villageCommonVO.setDescription(description);

            Elements elements4 = doc.select(".current");
            for (Element element : elements4) {
                Elements img = element.select(".J_lazyload");
                //输出图片链接
                System.out.println(img.attr("data-original"));
                list.add(img.attr("data-original"));
                villageCommonVO.setPicture(list);
                //System.out.println(img.attr("data-lazy-img"));
            }

        }
    }



    /**
     * 当URL下载完成会调用这个方法，可以获取下载页面的URL，文本，链接，HTML和唯一ID等内容
    @Override
    public void visit(Page page) {
        String url = page.getWebURL().getURL();
        System.out.println("URL: " + url);
        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            String html = htmlParseData.getHtml();
            Document doc = Jsoup.parse(html);
            List<String> list = new ArrayList<String>();
            Element element1 = doc.select("h1.clear,red").first();
            Elements elements1 = element1.getElementsByAttribute("href");
            list.add(elements1.attr("href"));//获取链接属性的值
            Element element2 = doc.select("div.center,fr").first();
            Elements elements2 = element2.getElementsByAttribute("href");
            for (int i =0;i<9;i++) {
                list.add(elements2.get(i).attr("href"));//获取链接属性的值
            }
            try {
                for (int j = 0;j<list.size();j++) {
                    Politic politic = new Politic();
                    Document document = Jsoup.connect(URL+list.get(j)).get();//爬取此时的URL数据
                    Elements elements3 = document.select("#rwb_zw");
                    politic.setContent(elements3.select("p").text());//内容
                    Elements elements4 = document.select("div.clearfix,w1000_320,text_title");
                    politic.setTitle(elements4.select("h1").text());//标题
                    String str = elements4.select("div.box01 .fl").text();
                    //String publishedAt = str.substring(0,4)+"-"+str.substring(5,7)+"-"+str.substring(8,10)+" "+str.substring(11,16);//时间格式
                    String publishedAt = str.substring(0, 17);
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy年MM月dd日HH:mm"); //定义时间格式
                    Date date = sf.parse(publishedAt);  //转换成date类型
                    politic.setPublishedAt(new Timestamp(date.getTime()));//时间 ：//date类型转换成Timestamp类型
                    politic.setSource(str.substring(21));//来源
                    if(ConnectionUtil.select(politic.getTitle()) == 0){//判断title是否重复:若为0(不重复)则插入数据
                        int  i = ConnectionUtil.insert(politic);
                        if(i == 0){//判断是否插入成功
                            throw new Exception("新增时政要闻资料失败！");
                        }
                    }else{
                        System.out.println("资料已经存在！");
                    }
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }catch (ParseException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
     */

}