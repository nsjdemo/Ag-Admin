package com.github.wxiaoqi.security.admin.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.admin.entity.User;
import com.github.wxiaoqi.security.admin.mapper.UserMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.constant.UserConstant;


/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-08 16:23
 */
@Service
public class UserBiz extends BaseBiz<UserMapper,User> {

    @Autowired
    private UserMapper userMapper;

    @Override
    public void insertSelective(User entity) {
        String password = new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT).encode(entity.getPassword());
        entity.setPassword(password);
        super.insertSelective(entity);
    }

    @Override
    public void updateSelectiveById(User entity) {
        super.updateSelectiveById(entity);
    }

    /**
     * 根据用户名获取用户信息
     * @param username
     * @return
     */
    public User getUserByUsername(String username){
        return userMapper.selectByName(username);
    }


    public Boolean phoneExist(String cellphone){
        return userMapper.phoneExist(cellphone)>0?Boolean.TRUE:Boolean.FALSE;
    }
    /*
    根据用户id查找用户名称
    * */
    public String getUsername(int userId){
        return userMapper.getUserame(userId);
    }
    /*修改密码*/
    public int updatePwd(int userId,String newPwd){
        return userMapper.updatePwd(userId,newPwd);
    }

    public Integer getIdByName(String username) {
        return userMapper.getIdByName(username);
    }
}
