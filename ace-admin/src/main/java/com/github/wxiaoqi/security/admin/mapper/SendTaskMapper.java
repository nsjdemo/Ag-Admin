package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.api.entity.SendTask;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author Mr.AG
 * @email 463540703@qq.com
 * @date 2018-05-29 10:34:50
 */
public interface SendTaskMapper extends Mapper<SendTask> {
	
}
