package com.github.wxiaoqi.security.admin.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.wxiaoqi.security.auth.client.annotation.IgnoreClientToken;
import com.github.wxiaoqi.security.auth.client.annotation.IgnoreUserToken;

/**
 * 缓存 Created by Jason on 16/5/23.
 */
@IgnoreClientToken
@IgnoreUserToken
@Controller
@RequestMapping(value = "/cache")
public class CacheController {


}
