package com.github.wxiaoqi.security.admin.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.wxiaoqi.security.admin.entity.User;

import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
    List<User> selectMemberByGroupId(@Param("groupId") int groupId);
    List<User> selectLeaderByGroupId(@Param("groupId") int groupId);
    long phoneExist(@Param("cellphone") String cellphone);

    User selectByName(String username);
    //获取用户名
    String getUserame(@Param("userId") int userId);
    //修改密码
    int updatePwd(@Param("userId")int userId,@Param("newPwd")String newPwd);

    Integer getIdByName(String username);
}