package com.github.wxiaoqi.security.admin.mapper;


import com.github.wxiaoqi.security.admin.vo.UserVo;

import java.util.Map;

public interface SystemMapper extends tk.mybatis.mapper.common.Mapper<UserVo> {

     Map<String,String> getuser(String id);

     int updateAdminUser(Map map);
}