package com.github.wxiaoqi.security.api.entity;

/**
 * Created by mac on 18/9/7.
 * use for  倪杰杰 app测试类
 */
public class AppUser {

    private Integer id;
    private String name;
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
