package com.github.wxiaoqi.security.api.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * ClassName: BaseConfigTable <br/>  
 * Function: 基础配置表类 <br/>  
 * date: 2018年04月26日 下午3:50:21 <br/>  
 * @author qszhang  
 * @version   
 * @since JDK 1.8
 */
@Table(name = "base_config_table")
public class BaseConfigTable {
	/**
     * 主键
     */
	@JsonInclude(JsonInclude.Include.ALWAYS)
    @Id
    @Column(name = "config_code")
    private String configCode;

    /**
     * 值
     */
	@JsonInclude(JsonInclude.Include.ALWAYS)
    @Column(name = "config_code_value")
    private String configCodeValue;
    
    /**
     * 创建时间
     */
	@JsonInclude(JsonInclude.Include.ALWAYS)
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
	@JsonInclude(JsonInclude.Include.ALWAYS)
    @Column(name = "update_time")
    private Date updateTime;
    
    /**
     * 有效性，1-有效；0-无效
     */
    @JsonInclude(JsonInclude.Include.ALWAYS)
    @Column(name = "valid")
    private int valid;

    /**
     * 备注
     */
    @JsonInclude(JsonInclude.Include.ALWAYS)
    @Column(name = "remark")
    private String remark;

	public String getConfigCode() {
		return configCode;
	}

	public void setConfigCode(String configCode) {
		this.configCode = configCode;
	}

	public String getConfigCodeValue() {
		return configCodeValue;
	}

	public void setConfigCodeValue(String configCodeValue) {
		this.configCodeValue = configCodeValue;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public int getValid() {
		return valid;
	}

	public void setValid(int valid) {
		this.valid = valid;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
    
}