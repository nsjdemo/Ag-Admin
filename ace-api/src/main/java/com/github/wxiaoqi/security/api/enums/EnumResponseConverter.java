package com.github.wxiaoqi.security.api.enums;

import com.fasterxml.jackson.databind.util.StdConverter;

/**
 *
 * Json解析转换器
 * Json响应枚举类型为明文
 */
public class EnumResponseConverter extends StdConverter<IEnum, String> {

    @Override
    public String convert(IEnum iEnum) {
        return null == iEnum ? "" : iEnum.getTitle();
    }
}