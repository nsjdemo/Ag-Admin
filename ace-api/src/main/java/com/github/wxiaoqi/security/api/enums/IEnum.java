package com.github.wxiaoqi.security.api.enums;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(converter = EnumResponseConverter.class)
public interface IEnum<T> {

    T getValue();

    String getTitle();

}
