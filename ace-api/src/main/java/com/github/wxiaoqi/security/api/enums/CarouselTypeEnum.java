package com.github.wxiaoqi.security.api.enums;

public enum CarouselTypeEnum implements IEnum<Integer> {

    INDEXPAGE(1, "首页轮播"),
    PROJECTLIST(2, "项目列表轮播");

    private int value;
    private String title;

    public static CarouselTypeEnum create(Integer value) {
        return EnumUtils.getEnum(CarouselTypeEnum.values(), value);
    }

    CarouselTypeEnum(int value, String title) {
        this.value = value;
        this.title = title;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public String getTitle() {
        return title;
    }

    public String toString() {
        return EnumUtils.toJSONString(this);
    }

    public static void main(String[] args) {
        CarouselTypeEnum type = CarouselTypeEnum.create(1);
        System.out.println(type);
    }
}
