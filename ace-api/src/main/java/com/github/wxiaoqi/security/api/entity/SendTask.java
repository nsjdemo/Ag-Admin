package com.github.wxiaoqi.security.api.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 
 * 
 * @author Mr.AG
 * @email 463540703@qq.com
 * @date 2018-05-29 10:34:50
 */
@Table(name = "send_task")
public class SendTask implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Integer id;
	
	    //用户id
    @Column(name = "user_id")
    private Long userId;
	
	    //用户家居id
    @Column(name = "user_house_id")
    private Long userHouseId;
	
	    //类型：1-家居；2-品类
    @Column(name = "type")
    private String type;
	
	    //下一次养护时间
    @Column(name = "next_time")
    private Date nextTime;
    
    	//1-数据已经同步插入到remind表
    @Column(name = "status")
    private String status;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户id
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：用户家居id
	 */
	public void setUserHouseId(Long userHouseId) {
		this.userHouseId = userHouseId;
	}
	/**
	 * 获取：用户家居id
	 */
	public Long getUserHouseId() {
		return userHouseId;
	}
	/**
	 * 设置：类型：1-家居；2-品类
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * 获取：类型：1-家居；2-品类
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * 设置：状态：1-数据已经同步插入到remind表
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态：1-数据已经同步插入到remind表
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * 设置：下一次养护时间
	 */
	public void setNextTime(Date nextTime) {
		this.nextTime = nextTime;
	}
	/**
	 * 获取：下一次养护时间
	 */
	public Date getNextTime() {
		return nextTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
