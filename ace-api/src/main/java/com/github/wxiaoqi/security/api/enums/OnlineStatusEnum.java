package com.github.wxiaoqi.security.api.enums;

public enum OnlineStatusEnum implements IEnum<Integer> {

    OFFLINE(0, "下线"),
    ONLINE(1, "上线");

    private int value;
    private String title;

    OnlineStatusEnum(int value, String title) {
        this.value = value;
        this.title = title;
    }

    public static OnlineStatusEnum create(Integer value) {
        return EnumUtils.getEnum(OnlineStatusEnum.values(), value);
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public String getTitle() {
        return title;
    }

    public String toString() {
        return EnumUtils.toJSONString(this);
    }
}
