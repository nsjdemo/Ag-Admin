package com.test.app.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.api.entity.BaseConfigTable;
import com.github.wxiaoqi.security.auth.client.annotation.IgnoreClientToken;
import com.test.app.service.BaseConfigTableService;

/**
 * 此controller主要是一个配置的controller，直接从数据库获取数据。
 * @author Administrator
 *
 */
@Controller
@RequestMapping("baseConfigTable")
public class BaseConfigTableController extends BaseController{
	
	@Autowired
	private BaseConfigTableService baseConfigTableService;
	
	@IgnoreClientToken
	@RequestMapping(value="/getBaseConfigTableValue/{baseConfigCode}",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getBaseConfigTableInfo(@PathVariable("baseConfigCode") String baseConfigCode) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		BaseConfigTable baseConfigTable = baseConfigTableService.findBaseConfigTableById(baseConfigCode);
		if(baseConfigTable != null) {
			resultMap.put("baseConfigCode", baseConfigTable.getConfigCodeValue());
		}else {
			resultMap.put("baseConfigCode", "数据库未配置该"+baseConfigCode+",请联系管理员");
		}
		return resultMap;
	}  
}
