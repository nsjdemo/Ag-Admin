package com.test.app.controller;

import com.github.wxiaoqi.security.auth.client.annotation.IgnoreClientToken;
import com.github.wxiaoqi.security.common.constant.CommonConstants;
import com.github.wxiaoqi.security.common.context.BaseContextHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * ${DESCRIPTION}
 *
 * @author qszhang
 * @create 2018-04-20 18:57
 */
public class BaseController {
	@Autowired
	protected HttpServletRequest request;

	Logger logger = Logger.getLogger(BaseController.class);

    public String getCurrentUserName(){
        String authorization = request.getHeader("Authorization");
        return new String(Base64Utils.decodeFromString(authorization));
    }
    
	public Long getCurrentSmartUserId() {
		if("noAuth".equals(request.getHeader("Authorization"))){
			logger.error("请求异常，smartUserId为空");
			return null;
		}
		if (StringUtils.isEmpty(BaseContextHandler.getUserID())) {
			logger.error("请求异常，smartUserId为空");
			return null;
		}	
		return Long.valueOf(BaseContextHandler.getUserID());
	}
    
	@IgnoreClientToken
	@RequestMapping("/loginmsg")
	@ResponseBody
	public Map<String, Object> loginmsg(){
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("code", CommonConstants.EX_USER_LOGIN_CODE);
		resultMap.put("msg", "please login.");
		return resultMap;
	}
}
