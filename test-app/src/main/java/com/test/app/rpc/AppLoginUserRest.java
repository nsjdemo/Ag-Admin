package com.test.app.rpc;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.web.bind.annotation.*;

import com.github.wxiaoqi.security.auth.client.annotation.IgnoreClientToken;
import com.github.wxiaoqi.security.auth.client.annotation.IgnoreUserToken;

/**
 * ${DESCRIPTION}
 *
 * @author qszhang
 * @create 2018-04-20 15:15
 */
@IgnoreUserToken
@IgnoreClientToken
@RestController
@RequestMapping("api")
public class AppLoginUserRest {

    @RequestMapping(value = "/user/phone/{phone}", method = RequestMethod.GET, produces = "application/json")
    public SecurityProperties.User getUserByPhone(@PathVariable("phone") String phone) {
        return null;
    }

}
