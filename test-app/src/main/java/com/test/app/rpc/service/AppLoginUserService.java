package com.test.app.rpc.service;

import com.github.wxiaoqi.security.api.entity.AppUser;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.test.app.mapper.AppUserMapper;
import org.springframework.stereotype.Service;


/**
 * Created by ace on 2017/9/12.
 */
@Service
public class AppLoginUserService extends BaseBiz<AppUserMapper, AppUser> {
}
