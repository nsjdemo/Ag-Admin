package com.test.app.mapper;

import com.github.wxiaoqi.security.api.entity.AppUser;
import tk.mybatis.mapper.common.Mapper;

/**
 * Created by mac on 18/9/7.
 * use for
 */
public interface AppUserMapper extends Mapper<AppUser> {
}
