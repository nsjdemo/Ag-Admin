package com.test.app.mapper;

import com.github.wxiaoqi.security.api.entity.BaseConfigTable;

import tk.mybatis.mapper.common.Mapper;

public interface BaseConfigTableMapper extends Mapper<BaseConfigTable> {

}
