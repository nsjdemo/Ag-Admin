package com.test.app.service;

import com.test.app.mapper.BaseConfigTableMapper;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.api.entity.BaseConfigTable;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

@Service
public class BaseConfigTableService extends BaseBiz<BaseConfigTableMapper, BaseConfigTable>{

	public BaseConfigTable findBaseConfigTableById(String BaseConfigCode) {
		return mapper.selectByPrimaryKey(BaseConfigCode);
	}
}
