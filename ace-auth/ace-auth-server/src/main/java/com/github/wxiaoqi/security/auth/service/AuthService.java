package com.github.wxiaoqi.security.auth.service;


import com.github.wxiaoqi.security.auth.vo.FrontUser;

public interface AuthService {
    String adminLogin(String username, String password) throws Exception;
    String villageLogin(String username,String password) throws Exception;
    String refresh(String oldToken);
    void validate(String token) throws Exception;
    FrontUser getUserInfo(String token) throws Exception;
    Boolean invalid(String token);
    String smartUserPhoneLogin(String phone, String identifyCode) throws Exception;
    void forgetToken(String phone, String identifyCode) throws Exception;
}
