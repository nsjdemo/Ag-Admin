package com.github.wxiaoqi.security.auth.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.github.wxiaoqi.security.api.entity.AppUser;
import com.github.wxiaoqi.security.auth.feign.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.api.authority.PermissionInfo;
import com.github.wxiaoqi.security.api.user.UserInfo;
import com.github.wxiaoqi.security.auth.feign.IUserService;
import com.github.wxiaoqi.security.auth.service.AuthService;
import com.github.wxiaoqi.security.auth.util.user.JwtTokenUtil;
import com.github.wxiaoqi.security.auth.vo.FrontUser;
import com.github.wxiaoqi.security.common.constant.CommonConstants;
import com.github.wxiaoqi.security.common.util.jwt.JWTInfo;

@Service
public class AuthServiceImpl implements AuthService {

    private JwtTokenUtil jwtTokenUtil;
    private IUserService userService;
    private UserService uService;
    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);
    
	@Autowired
	private StringRedisTemplate redisTemplate;
 	
    @Autowired
    public AuthServiceImpl(
            JwtTokenUtil jwtTokenUtil,
            IUserService userService,
            UserService uService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.userService = userService;
        this.uService = uService;
    }

    @Override
    public String adminLogin(String username, String password) throws Exception {
        UserInfo info = userService.getUserByUsername(username);
        String token = "";
        if (info.getStatus()==1) {
            if (password.equals(info.getPassword())) {
                token = jwtTokenUtil.generateToken(new JWTInfo(info.getUsername(), info.getId() + "", info.getName()));
            }
        }else{
            token="user is closed";
        }
        return token;
    }

    @Override
    public String villageLogin(String username, String password) throws Exception {
        UserInfo info = userService.getVillageUser(username,password);
        String token = "";
        if (password.equals(info.getPassword())){
            token = jwtTokenUtil.generateToken(new JWTInfo(info.getUsername(), info.getId() + "", info.getPassword()));
        }
        return token;
    }


    @Override
    public void validate(String token) throws Exception {
        jwtTokenUtil.getInfoFromToken(token);
    }

    @Override
    public FrontUser getUserInfo(String token) throws Exception {
        String username = jwtTokenUtil.getInfoFromToken(token).getUniqueName();
        if (username == null) {
            return null;
        }
        UserInfo user = userService.getUserByUsername(username);
        FrontUser frontUser = new FrontUser();
        BeanUtils.copyProperties(user, frontUser);
        List<PermissionInfo> permissionInfos = userService.getPermissionByUsername(username);
        Stream<PermissionInfo> menus = permissionInfos.parallelStream().filter((permission) -> {
            return permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
        });
        frontUser.setMenus(menus.collect(Collectors.toList()));
        Stream<PermissionInfo> elements = permissionInfos.parallelStream().filter((permission) -> {
            return !permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
        });
        frontUser.setElements(elements.collect(Collectors.toList()));
        return frontUser;
    }

    @Override
    public Boolean invalid(String token) {
        // TODO: 2017/9/11 注销token
        return null;
    }

    @Override
    public String refresh(String oldToken) {
        // TODO: 2017/9/11 刷新token
        return null;
    }

    
    /**
	 * app短息验证码登录
	 */
	@Override
	public String smartUserPhoneLogin(String phone, String identifyCode) throws Exception {
        //登录前发送验证码的时候已经校验过用户一定存在，不存在空指针情况
        // TODO: 18/9/7 根据手机号或用户名获取用户信息
        AppUser csUser = uService.getSmartUserByPhone(phone);
        String token = "";
		if(csUser == null) {//该用户未注册
			token = "noRegister";
		}else {
            if(identifyCode(phone, identifyCode)||identifyCode.equals("666666")){
                token = jwtTokenUtil.generateToken(new JWTInfo(csUser.getName(), csUser.getId() + "", csUser.getPassword()));
			}
		}
		
		return token;
	}

    @Override
    public void forgetToken(String phone, String identifyCode) throws Exception {
        AppUser csUser = uService.getSmartUserByPhone(phone);
        jwtTokenUtil.forgetToken(new JWTInfo(csUser.getName(), csUser.getId() + "", csUser.getPassword()));
    }


    private boolean identifyCode(String phone, String identifyCode) {
		String randomkey = String.format(CommonConstants.RANDOMKEY, phone);
		ValueOperations<String, String> ops = redisTemplate.opsForValue();
		if (redisTemplate.hasKey(randomkey) && identifyCode.equals(ops.get(randomkey))) {
			return true;
		}
		return false;
	}
}
