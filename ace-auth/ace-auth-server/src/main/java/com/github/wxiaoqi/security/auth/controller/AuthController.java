package com.github.wxiaoqi.security.auth.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.auth.service.AuthService;
import com.github.wxiaoqi.security.auth.util.user.JwtAuthenticationResponse;
import com.github.wxiaoqi.security.auth.vo.FrontUser;
import com.github.wxiaoqi.security.common.msg.AppResponse;

@RestController
@RequestMapping("jwt")
public class AuthController {
    @Value("${jwt.token-header}")
    private String tokenHeader;

    @Value("${jwt.expire}")
    private String expire;   //token有效时间

    @Autowired
    private AuthService authService;

    @Autowired
    StringRedisTemplate redisTemplate;

    /**
     * 管理品台登录
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "admin/token", method = RequestMethod.POST)
    public ResponseEntity<?> createAdminAuthenticationToken(@RequestParam("username") String username,
   		 @RequestParam("password") String password,@RequestParam("key") String key,@RequestParam("value") String value) throws Exception {

        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        if (ops.get(key).equals(null)){
            return ResponseEntity.ok(new AppResponse(0,"验证码已过期",false));
        }
        if (!ops.get(key).equalsIgnoreCase(value)){
            return ResponseEntity.ok(new AppResponse(0,"验证码错误",false));
        }
        final String token = authService.adminLogin(username,password);
        if(StringUtils.isEmpty(token)){
        	return ResponseEntity.ok(new AppResponse(0, "用户名密码错误", false));
        }else if (token.equals("user is closed")){
            return ResponseEntity.ok(new AppResponse(0, "该账号已关闭，请联系管理员开启", false));
        }
        return ResponseEntity.ok(new JwtAuthenticationResponse(token,expire,true));

    }


    
    /*
     * 仓山前台app登录
     */
    @RequestMapping(value = "app/cangshan/token", method = RequestMethod.POST)
    public ResponseEntity<?> createWelfareAppAuthenticationToken(@RequestParam("phone") String phone,
    		 @RequestParam("validCode") String validCode) throws Exception {

        final String token = authService.smartUserPhoneLogin(phone,validCode);
        
        if(StringUtils.isEmpty(token)){
        	return ResponseEntity.ok(new AppResponse(0, "验证码错误", false));
        }else {
        	if("noRegister".equals(token)) {
        		return ResponseEntity.ok(new AppResponse(0,"该用户未注册",false));
        	}else{
        		return ResponseEntity.ok(new JwtAuthenticationResponse(token,expire,true));
        	}
        }
        
    }

    @RequestMapping(value = "refresh", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(
            HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String refreshedToken = authService.refresh(token);
        if(refreshedToken == null) {
            return ResponseEntity.badRequest().body(null);
        } else {
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken,expire,true));
        }
    }

    @RequestMapping(value = "verify", method = RequestMethod.GET)
    public ResponseEntity<?> verify(String token) throws Exception {
        authService.validate(token);
        return ResponseEntity.ok(true);
    }

    @RequestMapping(value = "invalid", method = RequestMethod.POST)
    public ResponseEntity<?> invalid(@RequestHeader("access-token") String token){
        authService.invalid(token);
        return ResponseEntity.ok(true);
    }

    @RequestMapping(value = "user", method = RequestMethod.GET)
    public ResponseEntity<?> getUserInfo(String token) throws Exception {
        FrontUser userInfo = authService.getUserInfo(token);
        if(userInfo==null) {
            return ResponseEntity.status(401).body(false);
        } else {
            return ResponseEntity.ok(userInfo);
        }
    }
}
