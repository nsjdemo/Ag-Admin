package com.github.wxiaoqi.security.auth.feign;

import com.github.wxiaoqi.security.api.entity.AppUser;
import com.github.wxiaoqi.security.auth.configuration.FeignConfiguration;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "cangshan", configuration = FeignConfiguration.class)
public interface UserService {
    @RequestMapping(value = "/api/user/phone/{phone}", method = RequestMethod.GET)
    AppUser getSmartUserByPhone(@PathVariable("phone") String phone);

}
