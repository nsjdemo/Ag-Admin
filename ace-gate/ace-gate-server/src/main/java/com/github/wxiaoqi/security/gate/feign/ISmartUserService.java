package com.github.wxiaoqi.security.gate.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.wxiaoqi.security.api.user.UserInfo;
import com.github.wxiaoqi.security.gate.config.ZuulConfig;


/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-21 8:11
 */
@FeignClient(value = "cangshan",configuration = ZuulConfig.class)
public interface ISmartUserService {
  @RequestMapping(value = "/api/canghsan/phone/{phone}", method = RequestMethod.GET)
  public UserInfo getUserByUserName(@PathVariable("phone") String phone);

}
