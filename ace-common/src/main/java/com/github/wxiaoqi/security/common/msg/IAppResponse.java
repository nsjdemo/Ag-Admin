package com.github.wxiaoqi.security.common.msg;

/**
 * Created by ace on 2017/8/23.
 */
public class IAppResponse<T> {
	private String status ;
	private String message;
	private T result;

	public IAppResponse(String status, String message, T result) {
		this.status = status;
		this.message = message;
		this.result = result;
	}

	public IAppResponse() {
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

}
