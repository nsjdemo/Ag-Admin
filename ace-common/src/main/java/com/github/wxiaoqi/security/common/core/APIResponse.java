package com.github.wxiaoqi.security.common.core;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.wxiaoqi.security.common.enums.IBusinessStatusEnum;
import org.springframework.util.StringUtils;

import java.io.Serializable;

/**
 * 接口返回类
 */
public class APIResponse<T> implements Serializable {

    private String message;
    private int state;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;

    public String getMessage() {
        if(StringUtils.hasText(message)) {
            return message;
        }
        return "";
    }

    public APIResponse setMessage(String message) {
        this.message = message;
        return this;
    }


    public T getData() {
        if(null == data) {
            return (T)new BaseResp();
        }
        return data;
    }

    public APIResponse setData(T data) {
        this.data = data;
        return this;
    }

    public int getState() {
        return state;
    }

    public APIResponse setState(int state) {
        this.state = state;
        return this;
    }

    public boolean ifSuccess() {
        return state == 0;
    }

    public static APIResponse build() {
        return new APIResponse();
    }

    public static APIResponse build(IBusinessStatusEnum businessStatusEnum) {
        return build()
                .setMessage(businessStatusEnum.getDesc())
                .setState(businessStatusEnum.getCode());
    }

    @Deprecated
    public static <T> APIResponse build(IBusinessStatusEnum businessStatusEnum, T data) {
        return build(businessStatusEnum).setData(data);
    }

    public String toJson() {
        return JSON.toJSONString(this);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("APIResponse{");
        sb.append("message='").append(message).append('\'');
        sb.append(", status=").append(state);
        sb.append(", data=").append(data);
        sb.append('}');
        return sb.toString();
    }
}
