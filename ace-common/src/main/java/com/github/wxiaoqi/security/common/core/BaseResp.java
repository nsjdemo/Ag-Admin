/**
 * 
 */
package com.github.wxiaoqi.security.common.core;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * @ClassName BaseResp
 * @Description 返回基类
 */
@JsonInclude(JsonInclude.Include.ALWAYS)
public class BaseResp implements Serializable {
	private static final long serialVersionUID = 1L;

	public String toJsonStr() {
		return JSON.toJSONString(this);
	}

    @Override
	public String toString()  {
		return ToStringBuilder.reflectionToString(this, ObjectToStringStyle.getInstance());
	}
	
}
